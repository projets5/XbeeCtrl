#include "mbed.h"
#include "EthernetInterface.h"
#include "Websocket.h"
#include "XBeeLib.h"
#include <stdio.h>
#include <string.h>
using namespace XBeeLib;

DigitalOut camera1_green(p21);
DigitalOut camera1_yellow(p22);
DigitalOut camera1_red(p23);
DigitalOut camera2_green(p24);
DigitalOut camera2_yellow(p25);
DigitalOut camera2_red(p26);

Serial *log_serial;
char direction='\0';
char received_data[1024];
int camera;
int light;
/** Callback function, invoked at packet reception */
static void receive_cb(const RemoteXBeeZB& remote, bool broadcast, const uint8_t *const data, uint16_t len)
{
    const uint64_t remote_addr64 = remote.get_addr64();

    log_serial->printf("\r\nGot a %s RX packet [%08x:%08x|%04x], len %d\r\nData", broadcast ? "BROADCAST" : "UNICAST", UINT64_HI32(remote_addr64), UINT64_LO32(remote_addr64), remote.get_addr16(), len);


    for (int i = 0; i < len; i++){
        log_serial->printf("%c", (char)data[i]);
        direction = (char)data[i];
    }

    log_serial->printf("\r\n");
}

void lights_control(char data[]){
    int cnt = 0;
    char * pch;
    //printf ("Splitting string \"%s\" into tokens:\n",data);
    pch = strtok (data," :");
    while (pch != NULL)
    {
        if(cnt == 0){
            camera = atoi( pch );
            printf ("%d\n",camera);
        }
        if(cnt == 1){
            light = atoi( pch );
            printf ("%d\n",light);
        }
        //printf ("%s\n",pch);
        pch = strtok (NULL, " :");
        cnt++;
    }
    
    // 0 = CAMERA 1 , 1 = CAMERA 2 
    // 0 = RED, 1 = GREEN, 2 = YELLOW
    switch(camera) {
    case 0 :    switch(light) {
                case 0 : //RED
                        camera1_green = 0;
                        camera1_yellow = 0;
                        camera1_red = 1;
                        break;       
                case 1 : //GREEN
                        camera1_green = 1;
                        camera1_yellow = 0;
                        camera1_red = 0;
                        break;
                case 2 : //YELLOW
                        camera1_green = 0;
                        camera1_yellow = 1;
                        camera1_red = 0;
                        break;
                default: break; 
                }
                break;       
    case 1 :    switch(light) {
                case 0 : //RED
                        camera2_green = 0;
                        camera2_yellow = 0;
                        camera2_red = 1;
                         break;       
                case 1 : //GREEN
                        camera2_green = 1;
                        camera2_yellow = 0;
                        camera2_red = 0;
                         break;
                case 2 : //YELLOW
                        camera2_green = 0;
                        camera2_yellow = 1;
                        camera2_red = 0;
                         break;
                default: break; 
                }
             break;
    default: break; 
    }
    
}

int main() {   

    //WEBSOCKET
    // announce
    printf("STARTING Websocket\r\n");
    
    // Create a network interface and connect
    EthernetInterface eth;
    eth.connect();
    printf("IP Address is %s\r\n", eth.get_ip_address());

    // Create a websocket instance
    Websocket ws("http://192.168.0.103:8080/ctrlcenter/mbedws", &eth);
    int connect_error = ws.connect();
    
    //char buffer[1025];
    //memset(buffer, '1', 1025);
    //for(int i = 0; i < 10;i++){
    int error_c = ws.send("Xbee\r\n");
    //    if(i==9){
    //        buffer[1023] = '\n';
    //        buffer[1024] = '\r';
    //        buffer[1025] = '\0';
    //    }
    //    int error_c = ws.send(buffer);
    //}
    
    log_serial = new Serial(DEBUG_TX, DEBUG_RX);
    log_serial->baud(9600);


    //XBEE
    XBeeZB xbee = XBeeZB(RADIO_TX, RADIO_RX, RADIO_RESET, NC, NC, 9600);

    /* Register callbacks */
    xbee.register_receive_cb(&receive_cb);

    RadioStatus const radioStatus = xbee.init();
    MBED_ASSERT(radioStatus == Success);

    /* Wait until the device has joined the network */
    log_serial->printf("Waiting for device to join the network: ");
    while (!xbee.is_joined()) {
        wait_ms(1000);
        log_serial->printf(".");
    }
    log_serial->printf("OK\r\n");

    while (true) {
        xbee.process_rx_frames();
        if(direction != '\0'){
            int error_c = ws.send(&direction);
        }
        if(ws.read(received_data)){
            //printf("done reading %s\r\n",received_data);  
            lights_control(received_data);
        }
        wait_ms(300);
    }
    delete(log_serial);
}